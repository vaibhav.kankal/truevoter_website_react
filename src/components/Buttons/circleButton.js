import React from 'react';
import Styled from 'styled-components';

const StyledButton = Styled.button`
    display:none;  
    padding: 10px 15px;
    font-size: 19px;
    text-align: center;
    cursor: pointer;
    outline: none;
    color: #2c2d8a;
    background-color: transparent ;
    border-color: #2c2d8a;
    border-radius: 30px;
    border-style: solid;
 
    &:hover {background-color: #2b2652;color:white;}
  
    &:focus {outline: none;}
`
function button(){
    return <StyledButton>Sign Up</StyledButton>
}
export default button;