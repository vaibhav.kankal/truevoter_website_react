import React from 'react'
import Styled from 'styled-components'
import Btn from './button-css'

const Button = Styled.div`
    ${Btn};
    color:white;
    &:hover {box-shadow: 0 2px white}
`
function privacy(){
    return <Button>Privacy Policy</Button>
}
export default privacy;