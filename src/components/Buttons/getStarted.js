import React from 'react'
import Styled from 'styled-components'

import Button from './button-css'

const Btn = Styled.button`
    ${Button};
`
function button() {
    return (
        <div>
            <Btn>
                Get Started
             <img src="https://img.icons8.com/ios/24/000000/long-arrow-right.png" />
            </Btn>
        </div>
    )
}
export default button;