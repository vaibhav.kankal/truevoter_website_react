import React from 'react'
import Styled from 'styled-components'
import Btn from './button-css'

const Button = Styled.div`
    ${Btn};
    color:white;
    &:hover {box-shadow: 0 2px white}
`
function policy(){
    return (
    <Button>Acceptance Policy</Button>
    )
}
export default policy;