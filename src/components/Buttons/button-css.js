import { css } from 'styled-components';

const StyledButton = css`

    padding: 10px 15px;
    font-size: 15px solid;
    text-align: center;
    cursor: pointer;
    outline: none;
    color: black;
    background-color: transparent ;
    border: none;

  &:hover {box-shadow: 0 1px black}
  
  &:active {
    background-color: transparent;
    border:white;
  }
  &:focus {
    outline:none;
  }
`
export default StyledButton;