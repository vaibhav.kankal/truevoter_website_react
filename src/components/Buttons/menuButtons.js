import React from 'react';
import Styled from 'styled-components';
import Btn from './button-css'

const Button = Styled.button`
  ${Btn}
`
function button() {
  return (
    <div class="collapse  navbar-collapse navbar-right" id="collapsibleNavbar">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href={'/'}><Button>HOME</Button></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href={'/Voter-list-search'}><Button>VOTER LIST SEARCH</Button></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about-us"><Button>ABOUT US</Button></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://web.truevoter.in/#/login" target= '_blank'><Button>LOGIN</Button></a>
        </li>
        
      </ul>
    </div>
  )
}

export default button;