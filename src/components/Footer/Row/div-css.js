import { css } from 'styled-components';

const div1 = css`
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color:white;
`;
export default div1;