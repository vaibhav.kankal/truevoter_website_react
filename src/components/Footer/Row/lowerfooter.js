import React from 'react'
import Styled from 'styled-components'

import Btn from '../../Buttons/button-css'

const Button = Styled.button`
    ${Btn};
    color:#CCC;
    &:hover {box-shadow: 0 2px white}
`
const Heading = Styled.h6`
    opacity: 0.5;
    color: #CCC;
    margin-top:1rem;
    display : inline-block;
`
function lower() {
    return (
        <div className="row">
            <div className="col-md-6">
                <a href={'/privacy-policy'}><Button>PRIVACY POLICY</Button></a>
            </div>
            <div className="col-md-6">
                <Heading>© Abhinav IT Solutions Pvt Ltd - 2020. All rights reserved.</Heading>
            </div>
        </div>

    )
}
export default lower;