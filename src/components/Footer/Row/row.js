import React from 'react'
import Styled from 'styled-components';

const truevoter = '/Images/truevoter-logo.png';
import Div from './div-css'

const Wrap = Styled.div`
 ${Div};
 marginTop:-3%;
`
const P = Styled.div`
display: block;
margin-block-start: 1em;
margin-block-end: 1em;
margin-inline-start: 0px;
margin-inline-end: 0px;
opacity: 0.7;
line-height: 1.9;
font-family: cursive;

margin-left: 130px;
text-align: start;
// margin-right: 130px;
font-size: smaller;
box-sizing: border-box;
`
const StyledLogo = Styled.img`
float:left;
height: 90px;
margin-left: 35%;
margin-top: 50px;
`
const H4 = Styled.div`
margin-bottom: 25px;
font-size: 15px;
font-weight: 600;
letter-spacing: 2px;
text-transform: uppercase;
opacity: 0.8;
`
const Number = Styled.div`
font-family: cursive;
font-size: xx-large;
opacity: 0.7;
`
const A = Styled.div`
font-size: 10px;
font-family: cursive;
opacity: 0.7;
`

function row() {
    return (
        <>
        <div className="row">
        <div className='col-md-3'>
        <StyledLogo src={process.env.PUBLIC_URL+truevoter} /><br/>
        
        <Wrap>
            <P style={{marginTop:"120px"}}>We firmly believe in Quality Delivery with Best Return of Investment (ROI) yet Cost Effective Solutions.</P><br/>
            <P>Headquarters:25-31, Bandal Dhankude Plaza,Kothrud, Pune 411038</P><br/>
            <P>Phone: (91) 7767008611/12  Email:truevoter.sec@maharashtra.gov.in</P><br/>
        </Wrap>
        </div>

         <div className="col-md-3">
         <Wrap>
         <H4>BLOGROLL</H4>
         </Wrap>
       </div>
   
       <div className="col-md-3">
       <Wrap>
       <H4>RECENT POSTS</H4>
       </Wrap>
     </div>

      <div className="col-md-3">
     <Wrap>
     <Number>386,964</Number>
     <A> TOTAL DOWNLOADS</A>
    </Wrap>
    </div>
    </div>
    <div className="clearfix"></div>
     </>
    )
}
export default row;