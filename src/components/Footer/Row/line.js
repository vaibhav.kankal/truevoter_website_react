import React from 'react'
import Styled from 'styled-components'

const Line = Styled.div`

display: block;
margin-top:50px;
margin-bottom: 50px;
width:86%;
margin-left: auto;
margin-right: auto;
border-style: inset;
border-width: 0.1px;
border-color: #999;
border-style: solid;
opacity: 0.7;

`
function line() {
    return (
        <div>
            <Line />
        </div>
    )
}
export default line;