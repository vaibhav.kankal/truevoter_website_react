import React from 'react'
import Styled from 'styled-components'

import Row from './Row/row'
import Line from './Row/line'
import Lower from './Row/lowerfooter'

const Footer = Styled.footer`

    background-color: #333;
    color: #CCC;
    border-top-color: rgba(0,0,0,0.2);
    position: relative;
    /* background-color: #2fa9da77; */
    /* border-top: 5px solid rgba(0, 0, 0, 0.2); 46bbe977*/
  
  @media (min-width: 992px) {
    .sticky-footer #slider:not(.slider-parallax-invisible),
    .sticky-footer #page-submenu,
    .sticky-footer #page-title,
    .sticky-footer #content {
      z-index: 2;
    }
  
    .sticky-footer #footer {
      position: sticky;
      top: auto;
      bottom: 0;
      left: 0;
    }
  }
  & .footer-widgets-wrap {
    position: relative;
    padding: 80px 0;
  }
  
  .copyrights {
    padding: 40px 0;
    background-color: #dddddd;
    font-size: 0.875rem;
    line-height: 1.8;
  }
  .copyrights i.footer-icon {
    position: relative;
    top: 1px;
    font-size: 0.875rem;
    width: 0.875rem;
    text-align: center;
    margin-right: 3px;
  }
  .copyrights a {
    display: inline-block;
    margin: 0 3px;
    color: #CCC;
    border-bottom: 1px dotted #444;
  }
  .copyrights a:hover {
    color: #555555;
    border-bottom: 1px solid #666666;
  }
  .copyrights .text-right a:last-child {
    margin-right: 0;
  }
  
  .copyrights-menu {
    margin-bottom: 10px;
  }
  .copyrights-menu a {
    font-size: 0.875rem;
    margin: 0 10px;
    border-bottom: 0 !important;
  }
  .copyrights-menu a:first-child {
    margin-left: 0;
  }
  
  .footer-logo {
    display: block;
    margin-bottom: 30px;
  }
`
const Div = Styled.div`
  margin-top: 50px;
`
function footer() {
  return (
    <Div>
      <Footer>
        <Row />
        <Line />
        <Lower />
        <br />
      </Footer>
    </Div>
  )
}
export default footer;