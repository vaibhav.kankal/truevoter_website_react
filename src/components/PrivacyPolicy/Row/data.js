import React from 'react'
import Styled from 'styled-components'

const H1 = Styled.div`
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #333;
    font-size: 30px;
`
const Label = Styled.div`
    margin-top: 10px;
    font-weight: 300;
    color: #777;
`
const Line = Styled.div`
    content: '';
    display: block;
    margin-top: 30px;
    width: 40px;
    border-top: 2px solid #444;
    margin: 30px auto 0;
    margin-bottom: 50px;
`
const H3 = Styled.div`
margin-top: 50px !important;
    font-size: 16px;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin-bottom: 0;
    color: #333;
`
const P = Styled.div`
    margin: 6px 0 0 0;
    color: #999;
    font-size: 0.875rem;
`
const Section = Styled.div`
    position: relative;
    margin: 60px 0;
    padding: 60px 0;
    background-color: #F9F9F9;
    overflow: hidden;
`

function data(){
    return(
        <div className="col-md-12">
             <div className="col-md-12" style={{marginTop: "100px"}}>
            <H1>Privacy Policy</H1>
                <Label>Take your Voice up to all Contesting Candidates.</Label>
            <Line/>
            </div>

            {/* --------------------------- */}

            <Section>
                <div class="row topmargin-sm clearfix">
                    <div class="col-lg-4 bottommargin" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAD9ElEQVRoge2Z32scVRTHP2fGLitNI4ZKzWwWf/8FlfYlsSRNKoZifLK+KNpsIoJYLIj4AxwflFQQUfAH7qaKzUvii/UhaLPbgE0tCvpeaG2pyaYRE9AoiSa7x4fsbKZx3dzdu3EL3e/L3HvO+Z5zvuyduzNzoYH6Qmqd0Esf70byj4J0oBpfqyI/g56RHKMzBxKZWtarmYC2U5/cp+5qUlX2bRI66bgMTncmLtSibk0EeBPD7YieBFoMKQuo9GV7+qdsa1sL2PV18i7Xle+BnRVSFxyXvba/hGNDBnBdJ0XlzQO0aF5TtvWtBMROpfaDdlXLV5V9sUyyaj5YClCHx2z4hSSHbOi2S6jDko8KD9jwbQXELPmg2mZDtxXQZMm3zmG9C9UbDQH1RkNAvdEQUG80BNQbRgJa06m3vXRqyUsn39rqhmLp1JteOrUYSyffMIk3EiDwLBAFOWLVnQEUngeaFDlqEm+6hCIbrluJ6IZrWdwY98D1DFMBfwSD2ybfbyplt8DvwaBlfKQ5ZF80IZsJUK4Gw0humxfyzBjxy6OYIxr9qzVkv1oi9l8wEyDryXLi3hOyf2PELwNlPYfk9N6Qa9aEb7iE9FyRkNfe9YKMmvHLNSDFHCr0hlznSoSX4JsEKePFidDH2JgLUPjOOWnUaSkImZnu/jX+pH8T8HDgUgnVLNebSdCuW/NnQX4pTONey+KTxQQug8CCac8hzOfUGQwmravxp4DgBX9u1pn+1iSJkYAf7n96RciHHiPU3zk1vANgujNxAZU+YN6wcYB58vTNdR/+CdZ2HxH1A6cox+j0V2smAECX8h+yvjO0RZb1BL7vAGR7+qdcye0FOb1pIiGTw9mTPZA4C4DvO9HI8ggQ7G5Z2d78kWlfFX3cjaU/Pqg4XwY8gXdnpqaP4vv5Ykwm2YU6hxTtAOIF8xWFMw4yWlzzhea99vg7oM8VLOo4enC6a8Bo/VcsAMBLp4aAF0Omk39H5fFf2/uN/ngCtIyPNEe3LZ9A1m9cRIay+/tfqiRPxY8S2YXmV4BPQ6a+yLKe9zKpRLA7lcXYmNs6MTwQjSyfv6Z5OJ6d3/Fqpf1Udz6gKl4mdQzkhQ2eLPCFKOM5cS45SyuXc5GbRdzVO1xW71ZxelEeAVqvYYkMZbsOv4yI/j8CCrg9M/yQo/oBcGeVKS4rPDPbnfiq2h5qcELz2XbHXTki6ADmQi4pJPO5yHtzDz7xp0392p1S+r7jdcS7gU5UdwN7gFsK3t9QvsORH9H86ezUTCa8c9mg5sesAbyJpI/IawCovp7tGfA3oVSFG+aF5rpFQ8B/Z5alkuNal9mqxDl1PgcuAhcL4wZK4R/nUTCoEGARegAAAABJRU5ErkJggg=="/>
                        <H3>A VOTER’S VOICE</H3>
                        <P>A Voter’s Voice:To share expectation & problem of ward with the contesting candidate’s.</P>
                    </div>
                    <div class="col-lg-4 bottommargin">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAABmJLR0QA/wD/AP+gvaeTAAAHFElEQVRogc2af4xUVxXHP+e+2WW7xQX8we68ZSO1RROx1KQ1RNIom90plUoqsUUbxQAz0ChJg0ChVimDrS39AaU2GmFn2lgjTTZi/ZHya2fYgq1GbdMfpk1UStqw8xZKsjV03WV39r3jH8yWEd6bXzu7y/e/fefcc8/n3bv3nnvnCZeTVKU5lZjvicwXZA7oTFT6RfSsh/x9xDLpM60rTxUKIROVayF9ovtnU2u8ug2orgBmF3BV0DSexJ2bYi/5OUw6UDidWC7Kw0C4rIYqz7pezerTi77z3/zHkwfUHQ/Zbssu0LVjiPI6rlniLFp1cvTBpADN7n66bmjE3SfC4iqEe8N1axeMjpSpQsCyNLv76bph132uSjAA8yyT7Rj9Y0JHaBQGuLm4tw6KyNvqUYfwKYq8fEEWZNqjf5mwESoD5lWM3Bqe7k3LtMWudSKxOSOoDfoo4AU1UnQbTNAIlQojqrsy70/byLJlrp89nE4uFdXf4D9aLpbbNO5AJY+M6jYnsjpeLJ6dSj4GusE3BPLtcZ1y1YYBsDx2AupnM+jccQMaDxiAkzdFHeBd/1DSNC5A4wWTp+GA53XjAnTOzV5r0M3esJmJup8EE0EkrvACoytVhTDX7P/pFNAWX6PgTHil0HRkz1zjmS877bGfV9LeTiW+BjznZ1NYN+nFaVnqjodsd9bLwHV+ZqPWvFC1+5yZ6misEfmSp9gGGhXOqUqvivvaqRedV4jHAzfHYrLdlh2gvjDA2z2Rlf+oCtD1L++ucf5jVgqyApivihEurK0iimCwb5x1WtOJvd5IdvvpRd99r5w+7HRyK6p3BdkVeQqqUCnk5vQjwJwymvWryJbetuiuUpybUx1bFPlxsIdmGPQ+7Sy5c6ByoM5Oy/7o2QeBTZWGEOVXV2QHVh9ffNdQkE9zV+JHKtxfMJDyLScS2wtQ0ZQ7v8+c7QSWVNL+wzyE5QO19b3AZj97OJW8V9GCMAJPZHIwUMF5KG/THBNMntageslMsbs67hH0J0XaPp+xejbmPygLqLzzTMmaes2BJ2vzH9jpxGZEHirUSOBArWXdRmt8JP95yUBlwLyJ6CrLk2anPSauWzvVGL1FRI8GpHYs/3/I7krejbK9UAeq7L9ieGDpO60rz10SrThKGTAi2x1zcsvFby2XhTQfSdyrKluBmlznJ8D7SqZ9zb8A7HRiI8qjRdJ5vn544OtBC0lRoNIPZ6zPRGKPF4vXkt59tauhhSr0eSM1h0cvN5pTyfWK7ijS/I/1wwO3F1oVCwKVDINsyLRHdwI0HnrmSmOyN4sw3Vh6tKc1drxIkjR3Jb6vws6CTsofZrzfcPuby5YFVdq5XAJU+jTjbqct9hiAfST5GTw9AFyVs2ZFdFvmT5mHfEue87XZAwQs23n6/Yy+hmXFYM6n46PSzzOyyYlEP5zz4VSiW2Chj+cxUR4ZDg0dPdO6tr+5u2OWumYR6Hrgs0Vy/N2MvoZvlAIDPkBljMw9Tlvs4YvafUCFm7VvF8Jvm6a533zlhjuzpbb5/2W7Ox4aGnH3Ufyk+YN8GIB3Fq4YAgZKT7ewBPaVCwMXAdluy45iN5qK/NCJrL50nxBR4BfldB7ciTzbNN29o1wYyJse4XRieaHyHECULU4k+mCQvX544L7BmvqwCsvLTSQnFXRrpj36gHP+BZUtgfNLrWUN/5sCP2kIel+mfXXhqjencDq5TlTvB6aWkctbqt763siaQ2W0uUQGIBQaWkeh32dE4qXCAPS2RXe5bvZqFR4HThdwdYEXUaJOX8O8scJAboTsVOIEF/aOix2eyLTH1lXcQzxumm60rzdirkOxBeo8OI1Kj2bl2KnFq85UHNtH0nx4z+fVmFcD7K85fQ03BN01X44yiGkNtKrGK4WxU4nvNR5KfK7izCpUyBNm+dc/OlifHTxYSVC7qyMObLUsNJxKHBWRF1B5ybPkuOeODNYY0ySqU3raYn8bS/J+ColKGN8VUk4WqmqDZHd1xBHZOhpEYCGqC0ExLhgE9fRgjWUtHVvq/jKIBiVdU26wi2CCdLDWsnwPZ9WQQSToQ4bZLYeTdqmBLgcYAKPoWwE2cQ3rSwlyucAAiN29++O41inA8rF7KnJbb1vU93I8963BDihcMjFBMHBhYz0MRAJ8PJSd2ZC1Y/Q7m7mdnbV9H/vgFlGNA/OK9DFhMJADak4lv6jon4v4KvCuCP2qzAGmlBB/QmEg74BndyX3InpHFWNPOAzknYeyoXNrgDeqEVSV/ZMBA3lAZ1rX9uOarwKvjymi6JO9oZ5bJwMGfO4UGg89c6Vlsh0VTD9HkU297dFfVym3ihR4jWWn9yxAZRtIWyE/gRMKv8xaQzvPtK7tH5csy1DRm9OZqY7GkEqboF9QkQbgIyDvofzTGP3reBSYY9H/ALCXK8XP4e9kAAAAAElFTkSuQmCC"></img>
                        <H3>CANDIDATE AND VOTER’S COMMUNICATION TOOL</H3>
                        <P>Communication with Election personnel and registered voters.</P>
                    </div>
                    <div class="col-lg-4 ">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAEdElEQVRoge2YXWiTVxjHf8+bJq0Rqu7Kxshg5mqIc371Ylcz6XCiWMbwYrsQakIv/BgDWzcZjMGYtjhwRQa1b2XCvBhDtDg7WRO91PnZjW1XRRik0V1M3FiztkneZxc2MW8ak5PYMUbzg0DOeZ+P/8M55z3nvNCgQYMGDRYzshBBAhcH/SxtiuCwCXgZIYTqcmD5nMkjRB6hTAB3sbjFVDae2tmdftbc9RegKm3xodcssWIKrwP+GiOkBb511Bm6H4l9h4jWI6OuAgJjQ7sQ+Qh4qR7/MvyA6oepjthIrY41FdCWsJ/H4XMRtteayJBLKuy7H47+aupgXEAgYe9E+QJ4ri5p5vwpQnQyHP3axNgyMWqLDx9BGeHfFw/QqspXbfHhIybGVUdgVdw+ofCOYfJZ4DyiI4h1J5fxJgE83kwQdTYAnah0Aj4jcaonJjti71a0qfQwkBg+iup7RsngXBar97dI171KdqsTg2scPP2qvGESF5FjqfDe9yvkLU9gzO5CGDZIkRP08GQk9qmRoHz8hH0IpQ+TaazsTXVET5d7VLaAYHxonYNcw+DdLuihWsXnCcTtXqDPwHQasV5JhbvuzM9fwgtjg8um8dxCCFWLKnBuMhJ9s7gvNDrQnPYt6UblLYS1ACg/gZz1Z6ZOTWw/OFNSxHmgs2oJykQLuU33Orr/KO6eN3wz0tRvIh6YzWL1FncEE2dWpX3+70E+Q2gHlgJLEdoRHUj7/NeDiTOrXAI89PB48VdGCM1IU39pt6uAYMLeomjUQDzA+eIFGxodaHY0c4nKu/N6RzPfhEYHmvMdyVejE4DRDqxoNJiwtxT3PSlAVVTlJIZ7A6KupGnfkm7Mjhbr083+mFuYmB4hLFU5iWph6hfErrxyepuimw0DIaq3S0S8beqL4rIVi1vmrrp55ZXT2/LtQgGWqtHOl2fWk0m5RMCLNbivLW5kZHqyltyWamFfMJsuC49T3Mj9vaxuHQVHR+STWhx9OW+guK3wSw3uLtuWlpm2WnI7Ikfz/wsFPNjadVmQm6ZBVGRjcVtUvjRWIJx1xXp8kzN0lZsPtnZdzrefDJ2Iiuh+Soa3Aq7Nx5+ZOgWMG/iNr/i99VSlWBVwRHR/8e3NNfeS4egNQWyjUCqdqxODa/LNie0HZyzx7qByEeOWeHf8vHt3YeMKXrVDgu4ySSmInQxHbxT3zVs8zZrtnbt8V8Pn4HHtjMnwnskVD1vbEQ4A14G/5n7XEA6seNjangzvcb1xnBzHAW/VbMpEs2Z7S7uf+TCH0JMKR49XtStDIGEfRjlmYPrUw1zZ11cyEvsR5YCRCqUvMDbcY2RbRCBu96KYvfmUfeXEwwJeaIALloeeubPNUwletUNz08Zo3td9oclT45UyA1xQZERy2dsZXzYJ4J1tCqqnaSPQObdgq895FuBKmactPnxE0I9N7RcAVeSD+5G9VafY4visApAKRy+qsEGV0fq1VeWSCutMxcNi+7To4v/8cbeU//LzeoMGDRo0WNz8Aw1buFsS6g7UAAAAAElFTkSuQmCC"/>
                        <H3>OFFICERS CHOICE FOR ELECTION MONITORING</H3>
                        <P>To share expectation & problem of ward with the contesting candidate’s.</P>
                    </div>
                </div>
            </Section>
        </div>
    )
}
export default data;