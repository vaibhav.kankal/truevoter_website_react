import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

function table() {
    return (
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>TYPE OF USER </th>
                        <th>VISITOR</th>
                        <th>CUSTOMER </th>
                        <th>USER</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><label>WHAT DATA WE MAY COLLECT</label></td>
                        <td><label>1. Your IP Address;
                        2. Your location;
                        3. How you behave on the Website, (what pages you land on, how much time you spend, etc.);
                        4. What Device you use to access the Website and its details (model, operating system, etc.);
                        5. Cookies and Web Beacon data; and
                        6. Data from third parties who have your explicit consent to lawfully share that data.If you submit an inquiry through the Website, we will ask for your:
                        7. Name; and
                        8. e-mail. </label></td>
                        <td><label>1. The name and e-mail of your representative who signs up for a Service on your behalf; and
                        2. Credit Card/Debit Card/Other Payment Mode information to check your financial qualifications, detect fraud and facilitate payments for our Services.
                </label> </td>
                        <td><label>
                            Your name, e-mail;
                            Your IP Address;
                            How you behave in the relevant product environment and use the features;
                            What device you use to access the Website/App and its details (model, operating system, etc.);
                            Cookies and Web Beacon data;
                            Data from third parties who have your explicit consent to lawfully share that data;
                            SMSs that you send or receive on your Device from your existing and prospective customers (“Leads”) if you have opted for the same in the relevant section of the App;
                            Phone call logs to record your call activities with Leads (i.e. call time, duration, phone number of relevant Lead) if you have opted for the same in the relevant section of the App; and
                            E-mails that you send and receive for the purpose of reading them to sync with your Leads, if you have opted to sync your e-mail with your CueLab account using our E-mail Sync App.
                            If you do choose to activate SMS and Call Log parsing or E-mail Sync to improve your experience, you can be sure that we will never collect or store any information that does not pertain to Leads.
                        </label> </td>
                    </tr>
                    <tr>
                        <td><label>HOW AND WHY WE USE IT </label></td>
                        <td><label>We use this information to analyse and identify your behaviour and enhance the interactions you have with the Website.If you submit your details and give us your consent, we may use your data to send you e-mails/newsletters, re-target CueLab advertisements or re-market our Services using services from third-parties like Facebook and Google.</label> </td>
                        <td><label>We collect this data in order to help you register for and facilitate provision of our Services. We also use this data to enable you to make payments for our Services.  We use a third-party service provider to manage payment processing. This service provider is not permitted to store, retain, or use information you provide except for the sole purpose of payment processing on our behalf. If you give us your consent, we may send you newsletters and e-mails to market other products and services we may provide. </label></td>
                        <td><label>We collect this data in order to facilitate provision of our Services. We will occasionally send you e-mails regarding changes or updates to the Service that you are using. In the event you report an issue with a Service, we may also screen/video record your Device only when you use the App for a limited time period to help us better understand how to address the issue.
                        If you give us your consent, we may send you newsletters and e-mails to market other products and services we may provide.
                         We may also conduct anonymized usage behaviour analysis at the aggregate level to determine how the features of a particular Service are being used. </label></td>
                 </tr>
                </tbody>
            </table>
           
    )
}
export default table;