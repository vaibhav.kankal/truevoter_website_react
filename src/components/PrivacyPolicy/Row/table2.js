import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

function table2(){
    return(
        <div className="col-md-12">
            <table class="table table-hover">
            <thead>
                    <tr>
                        <th>TYPE OF FILE </th>
                        <th>COOKIES</th>
                        <th>WEB BEACON</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <label>WHAT THEY DO </label>
                        </td>
                        <td>
                            <label>Cookies are text files which are sent from our server and downloaded to your Device when you visit our Website.They are useful because they allow us to recognize your Device when you return. You can disable them through your browser should you so wish. You can find more information about cookies at: www.allaboutcookies.org</label>
                        </td>
                        <td>
                            <label>Web Beacon (also known as Clear GIF, Web Bugs or Pixel Tag) is a tiny picture file embedded on the Website/App that tracks your behaviour and navigation. It is similar to a cookie in what it does, but it doesn’t get downloaded on to your Device. You can find more information about Web Beacons at: http://www.allaboutcookies.org/faqs/beacons.html </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>WHAT THEY WON’T DO </label>
                        </td>
                        <td>
                            <label>1. Harvest any personally identifiable information; or 2. Contain any executable software, malicious code or virus. </label>
                        </td>
                        <td>
                            <label>FOR THE AVOIDANCE OF ANY DOUBT, WE SHOULD CLARIFY THAT IN THE EVENT WE ANONYMIZE AND AGGREGATE INFORMATION COLLECTED FROM YOU, WE WILL BE ENTITLED TO USE SUCH ANONYMIZED DATA FREELY, WITHOUT ANY RESTRICTIONS OTHER THAN THOSE SET OUT UNDER APPLICABLE LAW. Where such data is not being used by us to render Services to you, we shall explicitly seek your consent for using the same. You can choose to withdraw this consent at any time, here. </label>
                        </td>
                    </tr>
                    </tbody>
            </table>
        </div>
    )
}
export default table2;