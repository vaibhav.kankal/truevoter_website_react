import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

function table3(){
    return(
        <div >
            <table className="table table-hover">
            <thead>
                    <tr>
                        <th>RIGHT TO BE INFORMED </th>
                        <th>RIGHT OF ACCESS </th>
                    </tr>
            </thead>
            <tbody>
                    <tr>
                        <td><label>You have a right to be informed about the manner in which any of your personal data is collected <br/>or used which we have endeavoured to do by way of this Policy. </label></td>
                        <td><label>You have a right to access the personal data you have provided by requesting us to provide you with the same.</label> </td>
                    </tr>
            </tbody>
            </table>
            <table className="table table-hover">
            <thead>
                <tr>
                    <th>RIGHT TO RECTIFICATION </th>
                    <th>RIGHT TO ERASURE </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><label>You have a right to request us to amend or update your personal <br/> data if it is inaccurate or incomplete. </label></td>
                    <td><label>You have a right to request us to delete your personal data. </label></td>
                </tr>
            </tbody>
        </table>
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>RIGHT TO RESTRICT </th>
                    <th>RIGHT TO OBJECT  </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><label>You have a right to request us to temporarily or permanently stop processing all or some of your personal data.  </label></td>
                    <td><label>You have a right, at any time, to object to our processing of your personal data <br/> under certain circumstances. You have an absolute right to object to us processing your personal data for the purposes of direct marketing. </label> </td>
                </tr>
            </tbody>
        </table>
        </div>
      
    )
}
export default table3;