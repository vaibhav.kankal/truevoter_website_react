import React from 'react'

import Home from '../Home/row'
import FeaturesForVoter from '../FeaturesForVoter/row'

import TagManager from 'react-gtm-module'
import ReactGA from 'react-ga';

ReactGA.initialize('G-2BMVVJ1FV8');
ReactGA.pageview(window.location.pathname + window.location.search);

ReactGA.initialize('G-2BMVVJ1FV8', {
    debug: true,
    titleCase: false,
    gaOptions: {
      userId: 123
    }
  });

const tagManagerArgs = {
    gtmId: 'GTM-KZHDSTP',
    dataLayerName: 'PageDataLayer'
} 
TagManager.initialize(tagManagerArgs)


function home() {
    return (
        <div>
            <Home />
            <FeaturesForVoter />
        </div>
    )
}
export default home;