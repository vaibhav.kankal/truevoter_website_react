import React from 'react';
import Styled from 'styled-components';

const truevoter = '/Images/truevoter-logo.png';

const StyledLogo = Styled.img`
float:left;
height: 60px;
margin-left: 30px;
`
function logo(){
    return <StyledLogo src={process.env.PUBLIC_URL+truevoter} />
}
export default logo;