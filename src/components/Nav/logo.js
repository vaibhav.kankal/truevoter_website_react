import React from 'react';
import Styled from 'styled-components';

const Cue = '/Images/cuelab.png';

const StyledImage = Styled.img`
  height:40px; /*35*/
  marginTop:20px;
  float:left;
` 
function logo(){
    return <StyledImage src={process.env.PUBLIC_URL+Cue} />
} 
export default logo;        