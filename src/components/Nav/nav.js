import React, { useEffect, useState } from 'react';

import "jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import 'bootstrap/dist/css/bootstrap.min.css';

import LogoName from './logoName';

import MenuButtons from '../Buttons/menuButtons';

function Nav() {
    const [navScrollStyle, setNavScrollStyle] = useState(false);
    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
    });

    const handleScroll = () => {
        if (window.pageYOffset > 0) {
            setNavScrollStyle(true);
        } else {
            setNavScrollStyle(false);
        }
    }
    return (
        <div className="app" >
            <nav class={navScrollStyle ? 'navbar navbar-expand-md scrolled fixed-top ' : 'navbar navbar-expand-md fixed-top '} onScroll={handleScroll} >
                <a class="navbar-brand">
                    <LogoName />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon">
                        <img src="https://img.icons8.com/material/24/000000/menu--v1.png" />
                    </span>
                </button>
                <MenuButtons />
            </nav>
        </div>
    )
}
export default Nav;