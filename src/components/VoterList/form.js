import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

import Styled from 'styled-components'
import axios from 'axios';
import jwt_decode from "jwt-decode";

import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'

import { backend_url, device_ip } from '../Common/constant/links'
import LinearGrad from '../Common/linearGradLeft-css'
import Captcha from './Captcha/captcha'

const Linear = Styled.div`
    ${LinearGrad};
`
const H1 = Styled.div`
    font-family: 'Raleway', sans-serif;
    font-weight: 500;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #333;
    font-size: 30px;
`
const baseUrl = 'https://api.truevoters.in/True_Voter/';
const stateId = 27;
const list = [];
const distId = [];
const assembly = [];
const voterList = [];
const familyDetails = [];

const FormData = () => {
  const [formData, setFormData] = React.useState('');
  const [res, setRes] = React.useState([]);
  const [nameWise, setVisible] = React.useState(false);
  const [idwise, setIsVisible] = React.useState(false);
  const [iscaptcha, setisCaptcha] = React.useState(false);
  const [grid, setGrid] = React.useState(false);
  const [familyDetailGrid, setFamilyDetailGrid] = React.useState(false);

  const dispatch = useDispatch()

  React.useEffect(() => {
    if (localStorage.getItem('token') === null) {
      login().then(res => {
        localStorage.setItem('token', res.data)
      })
    }

    fetch(baseUrl + 'downloaddistrict?stateId=' + stateId).then(data => { return data.json() }).then(data => {
      setRes(data);
    })
  });

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value.trim()
    });
    if (res.length !== 0) {
      for (let i = 0; i <= res.response.length - 1; i++) {
        list.push({ 'districtId': res.response[i].districtId, 'districtName': res.response[i].districtName });
      }
    }

    if (e.target.value === "name") {
      setIsVisible(false);
      setVisible(true);
    }
    if (e.target.value === "id") {
      setVisible(false);
      setIsVisible(true);
    }
  };

  const handleSelectDistrictChange = (e) => {
    if (distId.length !== 0) {
      distId.pop();
    }
    for (let i = 0; i <= list.length - 1; i++) {
      if (list[i].districtName === e.target.value) {
        distId.push(list[i].districtId);
      }
    }
    if (assembly.length !== 0) {
      for (let i = assembly.length; i >= 0; i--) {
        assembly.pop();
      }
    }
    const getAssembly = async () => {
      return await axios.get(baseUrl + 'downloadassembly?districtId=' + distId[0]).then(
        response => {
          if (response.status === 200) {
            return response.data;
          } else {
            throw new Error(response.status);
          }
        })
    }
    getAssembly().then(e => {
      if (assembly.length === 0) {
        for (let i = 0; i <= e.response.length - 1; i++) {
          assembly.push({ 'assemblyId': e.response[i].assemblyId, 'assemblyName': e.response[i].assemblyName });
        }
      }
    })
    //for textbox
    setFormData({
      ...formData,
      [e.target.name]: e.target.value.trim()
    });
  }


  const getIp = async () => {
    return await axios.get(device_ip)
      .then(response => {
        if (response.status === 200) {
          return response;
        } else {
          throw new Error(response.status);
        }
      })
  }

  const login = async () => {
    if (localStorage.getItem('deviceId') === null) {
      getIp().then(ipr => {
        const encodedString = new Buffer(ipr.data).toString('base64');
        localStorage.setItem('deviceId', encodedString);
      })
    }
    const id = localStorage.getItem('deviceId')
    const password = "s6u3Fe2fB6kygM50/h9xBA==";
    return await axios.post('https://voterlist.truevoter.in/api/login', { "Username": id, "Password": password })
      .then(response => {
        if (response.status === 200) {
          console.log(response)
          return response;
        } else {
          throw new Error(response.status);
        }
      })
  }

  const handleSubmit = (e) => {
    if (formData["captcha"] !== childRef.current.fetchCaptcha()) {
      setTimeout(function () { window.location.reload(1); }, 2000);
      e.preventDefault()
      setisCaptcha(true);
    } else {
      if (formData["cardId"] !== undefined) {
        const id = formData['cardId'];
        const assembly = formData['assemblyName'];
        const assemblyId = String(assembly).padStart(3, '0');
        const getList = async () => {
          const token = localStorage.getItem('token')
          const header = {
            "Authorization": `Bearer ${token}`
          }
          return await axios.get(`
        https://voterlist.truevoter.in/Value/DownloadEpicIdWiseDetailsNew?AssemblyId=${assemblyId}&EpicId=${id}`
            , { headers: header })
            .then(response => {
              if (response.status === 200) {
                return response;
              } else {
                throw new Error(response.status);
              }
            })
        }
        getList().then(e => {
          const response = jwt_decode(e.data);
          var data = JSON.stringify(eval("(" + response.unique_name + ")"));
          var result = JSON.parse(data);
          for(let i=0; i<=result.length-1; i++){
            voterList.push({'id': i, "value" : result[i]});
          }
          if (voterList !== null) {
            setGrid(true);
            setFormData("");
          }
          if(voterList[0].value.LASTNAME_EN === null){
            setGrid(false);
            alert("No Data Found");
          }
        })
      }
      if (formData["cardId"] === undefined) {
        const firstName = formData['firstName'];
        const lastName = formData['lastName'];
        const id = formData['assemblyName'];
        const assemblyId = String(id).padStart(3, '0');

        const getList = async () => {
          const token = localStorage.getItem('token')
          const header = {
            "Authorization": `Bearer ${token}`
          }
          return await axios.get(`
          https://voterlist.truevoter.in/Value/GetNameWiseSearchNew?FirstName=${firstName}&LastName=${lastName}&AssemblyId=${assemblyId}&PartNo=0`
            , { headers: header })
            .then(response => {
              if (response.status === 200) {
                return response;
              } else {
                throw new Error(response.status);
              }
            })
        }
        getList().then(e => {
          const response = jwt_decode(e.data);
          var data = JSON.stringify(eval("(" + response.unique_name + ")"));
          var result = JSON.parse(data);
          for(let i=0; i<=result.length-1; i++){
          voterList.push({'id': i, "value" : result[i]});
          }
          if (voterList !== null) {
            setGrid(true);
            setFormData("");
          }
          if(voterList[0].value.LASTNAME_EN === null){
            setGrid(false);
            alert("No Data Found");
          }
        })
      }
      e.preventDefault()
    }
  };

  const handleFamilyDetailsSubmit = (e) => {

    const id = e.target.value;

    const lastName = voterList[id].value.RLN_L_NM_EN;
    const acNo = voterList[id].value.AC_NO;
    const assemblyId = String(acNo).padStart(3, '0');
    const partNo = voterList[id].value.PART_NO;
    const sectionNo = voterList[id].value.SECTION_NO;
    
    console.log(lastName, assemblyId, partNo, sectionNo);

    const getFamilyList = async() => {
      const token = localStorage.getItem('token')
      const header = {
        "Authorization": `Bearer ${token}`
      }
      return await axios.get(`
      https://voterlist.truevoter.in/Value/DownloadFamilyDetailsNew?RlnLName=${lastName}&SectionNo=${sectionNo}&AssemblyId=${assemblyId}&PartNo=${partNo}`
        , { headers: header })
        .then(response => {
          if (response.status === 200) {
            return response;
          } else {
            throw new Error(response.status);
          }
        })
    }
    getFamilyList().then(e => {
      const response = jwt_decode(e.data);
      var data = JSON.stringify(eval("(" + response.unique_name + ")"));
      var result = JSON.parse(data);
      for(let i=0; i<=result.length-1; i++){
        familyDetails.push({'id': i, "value" : result[i]});
      }
    })
    if (familyDetails !== null) {
      setGrid(false);
      setFamilyDetailGrid(true);
    }
   e.preventDefault()
  };

  const childRef = React.useRef();
  const data = useSelector(state => state.data);
  // console.log("Redux-store", data);
  return (
    <div className="col-md-12">
      <form id="create-course-form" >
        <Linear>
          <br /><br />
          {iscaptcha ? (<div class="alert alert-danger alert-dismissible fade show" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Danger!</strong> Invalid Captcha.
          </div>) : <></>}

          <H1>SEARCH YOUR NAME IN VOTER LIST</H1>
          <br /><br />

          <div class="form-group">
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optradio" id="name" onChange={handleChange} value="name" ></input>
                  Name wise
                </label>
            </div>
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" id="idwise" name="optradio" value="id" onChange={handleChange} ></input>
                  ID card wise vise
                </label>
            </div>
          </div>

          {
            nameWise ? (
              <>
                <div class="form-group">
                  <select class="input" name="districtName" onChange={handleSelectDistrictChange} required>
                    <option selected>Select District</option>
                    {list.map((e) => (
                      <option id={e.districtId} name={e.districtName}>{e.districtName}</option>
                    ))}
                  </select>

                </div>
                <div class="form-group">
                  <select class="input" name="assemblyName" onChange={handleChange} required>
                    <option>Select Assembly</option>
                    {assembly.map((e) => (
                      <option value={e.assemblyId}>{e.assemblyName}</option>
                    ))}
                  </select>
                </div>
                <div class="form-group">
                  <input type="text" name="firstName" required placeholder="First Name" class="" onChange={handleChange} className="input" />
                </div>
                <div class="form-group">
                  <input type="text" class="input" name="lastName" placeholder="Last Name" onChange={handleChange} className="input" required />
                </div>

                <div class="form-group">
                  <Captcha ref={childRef} />
                </div>
                <div class="form-group">
                  <input type="text" name="captcha" placeholder="Enter Captcha" onChange={handleChange} className="input" required />
                </div>
                <div class="form-group">
                  <button className="btn btn-info" onClick={handleSubmit} >Send Response</button>
                </div>
              </>
            ) : null
          }

          {
            idwise ? (
              <>
                <div class="form-group">
                  <select class="input" name="districtName" onChange={handleSelectDistrictChange} required>
                    <option>Select District</option>
                    {list.map((e) => (
                      <option id={e.districtId} value={e.districtName}>{e.districtName}</option>
                    ))}
                  </select>
                </div>
                <div class="form-group">
                  <select class="input" name="assemblyName" onChange={handleChange} required>
                    <option>Select Assembly</option>
                    {assembly.map((e) => (
                      <option value={e.assemblyId}>{e.assemblyName}</option>
                    ))}
                  </select>
                </div>
                <div class="form-group">
                  <input type="text" class="input" name="cardId" placeholder="Id Card No" onChange={handleChange} className="input" required />
                </div>
                <div class="form-group">
                  <Captcha ref={childRef} />
                </div>
                <div class="form-group">
                  <input type="text" name="captcha" placeholder="Enter Captcha" onChange={handleChange} className="input" required />
                </div>
                <div class="form-group">
                  <button className="btn btn-info" onClick={handleSubmit} >Send Response</button>
                </div>
              </>
            ) : null
          }
          <br /><br />
          {
            grid ? (
              <>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>AC NO</th>
                      <th>PART NO</th>
                      <th>SR NO </th>
                      <th>Voter Name</th>
                      <th>Voter Name En</th>
                      <th>ID CARD NO</th>
                      <th>GENDER</th>
                      <th>AGE</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {voterList.map((e) => {
                      return(
                      <tr key={e.id}>
                        <td><label>{e.value.AC_NO}</label></td>
                        <td><label>{e.value.PART_NO}</label></td>
                        <td><label>{e.value.SLNOINPART}</label></td>
                        <td><label>{e.value.FM_NAME_V1} {e.value.LASTNAME_V1}</label></td>
                        <td><label>{e.value.FM_NAME_EN} {e.value.LASTNAME_EN}</label></td>
                        <td><label>{e.value.EPIC_NO}</label></td>
                        <td><label>{e.value.GENDER}</label></td>
                        <td><label>{e.value.AGE}</label></td>
                        <td>
                        <label> 
                          <button className="btn btn-info" value={e.id} onClick={handleFamilyDetailsSubmit}>Family</button>
                        </label>   
                        </td>
                      </tr>
                    )})
                    }
                  </tbody>
                </table>
              </>
            ) : null
          }
          {
             familyDetailGrid ? (
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>AC NO</th>
                      <th>PART NO</th>
                      <th>SR NO </th>
                      <th>Voter Name</th>
                      <th>Voter Name En</th>
                      <th>ID CARD NO</th>
                      <th>GENDER</th>
                      <th>AGE</th>
                    </tr>
                  </thead>
                  <tbody>
                    {familyDetails.map((e) => {
                    return(
                      <tr key={e.id}>
                        <td><label >{e.value.AC_NO}</label></td>
                        <td><label >{e.value.PART_NO}</label></td>
                        <td><label >{e.value.SLNOINPART}</label></td>
                        <td><label>{e.value.FM_NAME_V1} {e.value.LASTNAME_V1}</label></td>
                        <td><label>{e.value.FM_NAME_EN} {e.value.LASTNAME_EN}</label></td>
                        <td><label>{e.value.EPIC_NO}</label></td>
                        <td><label>{e.value.GENDER}</label></td>
                        <td><label>{e.value.AGE}</label></td>
                      </tr>
                    )}) 
                    }
                  </tbody>
                </table>
            ) : null
          }
        </Linear>
      </form>
    </div>
  )
}
export default FormData;