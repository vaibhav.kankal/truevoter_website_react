import React from 'react'
import Styled from 'styled-components'

import css from '../Common/topImage-css'


const Img = '/Images/image.png';
const iphone = '/Images/iphone2.png';

const Phone = Styled.img`
    margin-top: -55%;
    width: 250px;
    max-width: 100%;
    vertical-align: middle;
    border-style: none;
`
const Div = Styled.div`
    margin-top: -55%;
    width: 250px;
    max-width: 100%;
    vertical-align: middle;
    border-style: none;
`
const Image = Styled.img`
 ${css};
`
function image(){
    return (
        <div className="col-md-12">
            <Image src={process.env.PUBLIC_URL+Img} />
            {/* <Phone src={process.env.PUBLIC_URL+iphone}/> */}
        </div>
    )
}
export default image;