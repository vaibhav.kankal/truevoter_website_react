import React from 'react'
import Styled from 'styled-components'

const H1 = Styled.div`
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #333;
    font-size: 30px;
`
const Label = Styled.div`
    margin-top: 10px;
    font-weight: 300;
    color: #777;
`
const Line = Styled.div`
    content: '';
    display: block;
    margin-top: 30px;
    width: 40px;
    border-top: 2px solid #444;
    margin: 30px auto 0;
    margin-bottom: 50px;
`
const H3 = Styled.div`
margin-top: 50px !important;
    font-size: 16px;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin-bottom: 0;
    color: #333;
`
const P = Styled.div`
    margin: 6px 0 0 0;
    color: #999;
    font-size: 0.875rem;
`
const Section = Styled.div`
    position: relative;
    margin: 60px 0;
    padding: 60px 0;
    background-color: #F9F9F9;
    overflow: hidden;
`

function data() {
    return (
        <div>
            <div className="col-md-12" style={{ marginTop: "100px" }}>
                <H1>ABOUT US</H1>
                <Line />
            </div>

            <Section>
                <div class="row topmargin-sm clearfix">
                    <div class="col-lg-4 ">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAADJ0lEQVRoge2YQWgcVRiAv3+3CTHaiBWbZg0FcxeUCKURtGU3UiQpPZRCoTXobMyhYm0LHnqQgmgvLe3BFJLNpqVIK0IvhbZ0u0kPhggS9CIKiqImziYIFULblJh9vxclkzQ7O7P7dlvCfLCH/d97/3z/vtmZ9x5ERERERFSB+DUmxkZ6UYaBLT7d/kCk3006ObtqwYj5tirn8JcH2IrqeWtGIfEvANoD5klUK1Ip5Qp47Fn3BZi6WFRBuQL+rotFFfgXIPJXnTwqpsxjVH+uk0fFlCtgsk4eFVOmAPkqSBKF41ZsKsC3AHdy5mtg2qeLinK0kEqftKsVHP8ZOHHCAJdKtBZB+v/sTp+xbhWCsi+yuBQzwNKq8D+IHnRTTrY2WsEpW8B0cuAXgS9Whb93k/2Xa+QUikBLCaP6KStn4eUtudHXa6MUjkAFFLr7fxQYXDEwZs4/d3vwqdpoBSfwYm5hsekjoOAJvdBQbDxrXykcgQu48+aBeSPiALocFactP3K4Bl6BCbWcnk06NxD9zBsTOP18frjHrlZwQu8HGmMbPgSmPKG4Il+238rssKcVnNAF/Lbz7QcNS8XdwMxyVJ4wIlfbx0e3WXQLREU7st93DRTEmF5g3hPeaIzJtY2NvGZHLRi+xyrlSIwNd6Gxm4D3cXrfiOydTTo3/Ma25bPHBf3kv6+LwL01uv2KygdutzNRKk9Ve2I3+e6kMbEe4K4n3BxTvZrIZw6VGpfIZ973yAM0As+s8elE9KKfQ1Uz8D9tuewrEuMa6GZvXGEo3txyZKZr38KyfNYBzYS5tptKl+xr5VSi8IYzFZelLuAnb1xgwNyf/y6RG3kVIDGW2Q86hKUfDpuJADpuDT39QOIXgD0Pt+q3IC8CDWHz+s2A1QIAUJXEePYYysdAk42UNb+FViCibjJ9yhDrVPjGev5V1Oxkbjb1zg+FiZntotIHzNXqOvZvoTXYdP3zlqaGhaOIvAc8G3Z8ff8DPrTevPhkPL7Yh/IWQtBlx5ybSpc84q9rAV5a86Mdccwuge0IL6nK5tXvEWBOVQ4Vup0rj0QyIiIiYv3zL5Tp205VAsjiAAAAAElFTkSuQmCC" />
                        <H3>PHONE</H3>
                        <P>(91) 7767008611/1</P>
                    </div>
                    <div class="col-lg-4 bottommargin" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAGRUlEQVRoge2Zb2xbZxXGf+de12nDmhJ3H0hsokxI0FKmVWOFgpaVLE6KhGiriVlio5oaO4mohBhT1XRo7D+a0n4YElJF86dldJOqTEht2aZsczBbpqkqohSq0sKHUqXOzRAQt4E1q+N7Dx9iO/b1zRqncTSkPNKV7j3nPed9Hr+vz319DMtYxjKWsYxl/B9D5nI0Jo6sTNuZTkUeFtgAfGoJeQF8qHBe0Ff8pq/3cvOuj7wGeQoIDb8UdHT6NWBjRSnOH2dNR751pS1quR2G27BhcNDv6PTrfHLIA2y0DX19w+Cg3+0oEZCq/U8XcNeS0CoPG1NrJzvdxhIBKvq9peGzACgPu00lAgSuLQ2bBUCZdJtKBFjhWBsYrcCfloTU/HBBhIjVGtvqdpQIALDC7XHrveTdIkSAv1ec3txIKtplTdTcOdYSe9VrQGkZHRw0iUTs3GNj4sjKdMb5AaKPA7WV41qEFCov+H3Gz4vqv4sbeKxAfWDyXHC4/8Hc8+XmXR9ZrdEDvqr054Ae0KkKEk8L0uukjS9YrdEDheTr44fD9YHJP7gDSlagPt6v2dtTiu4dD3eMFPqDib4QtvETRdsB3yIRd4Bf27Z2/2NrR9GW/Uy8d5OB0QM0A1jhWBHnjxOQhcYN9T2WbN11rmjcm4fWYZrPAg9yS9C4GMaesfujRUUjGO/9vGI8D3ynkOcCBACQEeSw4fCM+3UefLtvs4r0APeVRRtOm6rdydaO3xXNnzh0O7a5B/gRUPLmdQvwrEIe8CnaaRv6t7rh/ucCb7xck3OMtXacslqi31CRBxC5eNNMIhdV5IHxluhmN3mAafw+w9B3UX0B+OtN07kNc6yAO+qfqD5fO7HmF+cjkXTennjaV2cH2wV5Cqh3RVmKPjNujh2m+enMTefIom64/z6UAwJfgYVvoTl0cElFn7Dujx1DJB9X/5tD1brK96igewEU2S9TmZ9Z3+66nhsTGuoPOCY7ELYB64BQ1pUELiB60uefPj7atDuFqtQPD3wfeNEKx6oWTcAs9Aywzwp3vF2UK3HodgCruetfeeLvD67SqWuPqcpeoIaPxzVF9pvVq19Mfj0yVRfva3JXxUUSkIPGEbPbamk/4+X97FsD9Y7BcUU3lZn4LGpvt1q7Rt2ORRYAgINwzM7oE4U1PZjoC6nNKZDgAvMmDVmxOdnyyFihcb5VqBwYKA+Zplysi/f/ELLbxubELZAHCDk6fbwxcWRl8WSVg18d4yyAXr+2B+TuRch5TzqTebTQUEEBeu6DtvZ3QkP9AUX2eI+R3yJ0YNvrbdt/27R5YzW2vV5VOoGEd4jsaxg5mD9ULtZZpnQeeBXAMdmBq9oIXLIdo/2DtvZ3PEIvZq++YHygWdEB4I4C/xo7XbUd+CVUUACOvJ9lu63QLMjvV5hG21h411WYaSKkApPPATsBBY7WTtQ8eT4SSY+Fo4mGkYNfztzwvwXck8uh6mzLCajYFjKQC9nb9bNWPWdW3dh6uXmGPEAqMPkssBeoY+bt3Z21ATDatDtlZNgKnJ/NI1+cnadCMFcYE9nbuvy0hrFztGl3yjV0p0d4kS35zdiEOE5hsyF/TPES8N8yuc4fGXvOTuBCUSpAC5dq4bCnnUD2djxvNI2joaH+gGvo0ZJgkV8VPgbjL61Vw3i5wJQ/0nusgLxSNlsPOGhu7/8lZ1PlS+qTocbEkU/nbLUTNU8CPVlSFtBT++/VT+X8DSMHa5XpN5npz84wlNkPuaQKVU9/2HvdX93OLbYW1ZR7gWFET6KyI29HN6Vt+0wwPhAdC0cT2eP4vuwFzH689fHD4cwNpw9oLE7OybwYr8kXqbn7Zyscuys01B9wfFwC1niMSSh6TDHfs82pUQDTXtUgOE2G6HdVZYtHzFW/ad6Rq2Rzfqk2DA76U2snO1EeAu4EbitXgQpbxlti79bFB34s6E/LjZ8D3VY4tj/3sOhVwQsz/zXYIxS8jBYChdNVprmlsN2yJAIgvy1PMfvLq1xcMR3Z7G4qVPI0WoRkyyNjpiNfVTi9gPA/ova98/qDo5K40ha1qkxzC6qPM78u+FWguzp9/Wtev8ZgCbeQGw0jB2vtdNV2Vd2OyDpUZ7aWSBJ1LghyYoVpnig8Ny3jk4j/Aci5aUVXejtrAAAAAElFTkSuQmCC" />
                        <H3>EMAIL</H3>
                        <P>truevoter.sec@maharashtra.gov.in</P>
                    </div>
                    <div class="col-lg-4 bottommargin">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAFWklEQVRoge1ZXYwTVRT+zkwXSAzy88CyM1UJJohGX4gmoPizbReREERMqlEB3XYBUfCBRCMmuvHBoOCLgMC2BTcmhkQSUGKE7XQXDdFgIAENgSBRgW3LjwHiT8Auc48PbZdpt+3ee7vAy37JJL13znfOd3rvnXvmDjCMYdwQ0I1wanUn7iFXBIRBFjH8AMCEXkNwBsSpdGjJiaGOOWSJTOrZNion3NcBioB56iDmxwicaDB9G/9ofuXqUMQfkkTsZCLMxOsA3KFIPU3gVelQ2456NZh1sZnJmulvB2EDgDEaHsYAFB696Onxf0+e1oV9+1hXiqFLBDPZTqITRO9p+7jubGXTTHtbPR60E7G746uZsLCe4F4QaJGVir+lz9eA7SRmMHg/6hnRyhAGYUZvMPqTKlFLiAB/oMsdBIZgrNUiqhIsJ9ZCwBM6wSTxmN0VD6qSdP7V5zQ4ajAorExRsm5vNwCaK0+gboDfyF/okWUxeF4+ljx8KsbWI01+AI2SatZlQq1vgii/NzCvt1Jb1wK8SoI90Zpxp50BzshqU8raMBuaJE3Pjbt0+zv9SQAAEY+7OHo1QOelYvmEbKy8vYqxAMuNBujw0XA4V957NBzOgfmwVCyiiSra1NYIQ7LA4+o1F0nWY0JckYuVh1IiRDgnaXqfvzs2p7zTdjrmArhXShj7zqpoU1rsaTpz1IL/XwC3DWYrBG23nNjbpjB2AoBLeJbzG6kM/un1nTqmok25RLGS8a9AmKfKUwPvzITaFqgwNDZE3qrOUQNpxFAvGpnJSsUPAjRNmSuHQ5lg5KGSR7cE1EeEiA3DXA6gT5k7OPoMwnLVJADNCrY30HqAgSF4oSoDYbVOCQ/UUYpn9/d+CNBeXX4FfJMJRD7WJeu/U7S3C1y5tgDAfm0fRTAO9Jn/Pa8zpYqo+xTFvyc+Xvh4H0APaLo44huZaz796PJL9egYNJHJyS1jrpD5LnmOeoi5J93StqnYbty7aYLP15Bixv1q4fkXkTODZ+e0Xij22MnYq0zU7JF4+mpu5PsX57z0V12JWE5iA8CvDdRAKzMtkfXFZuPeTRNM0+cojMwRmG4o07z0z/5YycQKEH8yMBY2ZFqiK2o5q5nIpJ5to3KumwUwtsJtZvCybKito9gxwYk1NoB+YGByzRQYJ0Wf8bB3JJqc2BICba6i6fII02yqdSpZc7HnXHd+WRK7AXxZuHYQKGA7HVOKN8+H2s6RMGeDcAFVQedNw51dMp2cjikECgDY4fG/20MaW9BS3Wutm5YT/xbA7LwlHc8EI1KV68SurY8bhujGwD/KZUIgG4x+L+PHSiWOec6R92RC0aeq2VYdEX+q0wbQ0t8hRKdMcAA4O6v1O2buqHBri2wSFWK2FDRVRNVEhMgtxPWzYdcwRnwuLQAADFoDwLsvCLjGGiUX+ZhuoWkWNFW2reFlsaeV7A0uTquIyAajpwD87Ok6knmyVfowAQAKMZNVNJWgYiJ2Mja97BuH9LQqBf9W/EWE3/V8eGIzT7WTsemVjComIohe9jQvjzDNXVoSyPPkYXyt46IQ+3IVbZ5QZSjfOxjYkg1Fl+mIAICmrsSD8AnOBtoOaftw4psJWFpoVtxTBryz58S1ZwDq3zsM5s90BQBAdlbkYD18ADBAnQwuJlLcU7aX2pSDSxbUiXQoeqBeIfUiHYr8CKLjnq4Bi75kavlTnbbgvlO4/tjdRYQvbqBGaTDjBQDF3d01qOEu75O0ZGoJ7nsRpd8V5zOjZmlwi2AWtH5U7CiZWgS6+6ZL0kR5YVqWiNgIxsmbK0kLv5rgT2+1iGEMYxi3AP8Dcy3RRc23EuEAAAAASUVORK5CYII="></img>
                        <H3>ADDRESS</H3>
                        <P>Headquarters:25-31,</P><P>Bandal Dhankude Plaza,Kothrud, Pune 411038s.</P>
                    </div>
                </div>
            </Section>
        </div>
    )
}
export default data;