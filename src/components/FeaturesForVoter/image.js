import React from 'react'
import Styled from 'styled-components'

import CommonImage from '../Common/commonImage-css'

const Lead = '/Images/LEAD CUSTOMER MANAGEMENET ILLUSTRATION.png';

const Image = Styled.img`
 ${CommonImage};
`
const Div = Styled.div`
    margin-top:30%;
`
function img(){
    return(
        <div className="col-md-6">
            <Div/>
            <Image src={process.env.PUBLIC_URL+Lead} />
            <Div/>
        </div>
    )
}
export default img;