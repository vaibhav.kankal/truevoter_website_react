import React from 'react'
import Styled from 'styled-components'

const H1 = Styled.div`
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #333;
    font-size: 30px;
`
const Label = Styled.div`
    margin-top: 10px;
    font-weight: 300;
    color: #777;
`
const Line = Styled.div`
    content: '';
    display: block;
    margin-top: 30px;
    width: 40px;
    border-top: 2px solid #444;
    margin: 30px auto 0;
    margin-bottom: 50px;
`
const H3 = Styled.div`
    margin-top: 50px !important;
    font-size: 16px;
    font-weight: 700;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin-bottom: 0;
    color: #333;
    text-aligin: start;

`
const P = Styled.div`
    margin: 6px 0 0 0;
    color: #999;
    font-size: 0.875rem;
    text-aligin: start;
`
const Section = Styled.div`
    position: relative;
    margin: 60px 0;
    padding: 60px 0;
    background-color: #F9F9F9;
    overflow: hidden;
`
const Div = Styled.div`
    text-align: start;
    margin-left: 10%;
`
const Div2 = Styled.div`
    text-align: start;
    margin-left: 10%;
    color: green;
`

function index() {
    return (
        <div className="row">
            <div className="col-md-12" style={{marginTop: "15px"}}>
                <Section>
                    <div class="row topmargin-sm clearfix">
                        <Div class="col-lg-4 ">
                            <H1>BLOG</H1>
                            <P>Our Latest News in Grid Layout</P>
                        </Div>
                    </div>
                </Section>
            </div>
            <div class="col-md-12">
                <Div2 class="col-md-4">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link1" href="#">{'<<'}</a></li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link1" href="#">2</a></li>
                    <li class="page-item"><a class="page-link1" href="#">3</a></li>
                    <li class="page-item"><a class="page-link1" href="#">4</a></li>
                    <li class="page-item"><a class="page-link1" href="#">{'>>'}</a></li>
                </ul>
                </Div2>
            </div>
        </div>
    )
}
export default index;