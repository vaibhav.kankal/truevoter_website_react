import { css } from 'styled-components'

const Img = css`
 height:100%;
 width:100%;
 position: relative;
 z-index:-1;
 margin-top:-1px;
`
export default Img;