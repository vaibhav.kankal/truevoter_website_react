import { css } from 'styled-components'

const Img = css`
 height:auto;
 width:80%;
 position: relative;
 z-index:-1;
 margin-top:-85px;
`
export default Img;