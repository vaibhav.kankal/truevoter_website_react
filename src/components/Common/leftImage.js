import React from 'react'
import Styled from 'styled-components'

const Img = 'Images/Group.png';

const Image = Styled.img`
    left:-13%;
    position:absolute;
    width:40%;
    opacity:0.5;
`
 function right(){
    return <Image src={process.env.PUBLIC_URL+Img} />
 }
 export default right;