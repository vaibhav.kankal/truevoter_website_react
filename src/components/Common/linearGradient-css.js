import { css } from 'styled-components'

const linear = css`
background: linear-gradient(to right,  rgba(252, 247, 247) , white);
padding:50px;
`
export default linear;