import React from 'react'
import Data from './data'

function row(){
    return(
        <div>
            <div className="row">
                <Data/>
            </div>
            <div className="clearfix"></div>
        </div>
    )
}
export default row;