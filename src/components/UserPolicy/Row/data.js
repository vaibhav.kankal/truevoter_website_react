import React from 'react'
import Styled from 'styled-components'

import Left from '../../Common/leftImage'
import Right from '../../Common/rightImage'

import LinearGrad from '../../Common/linearGradient-css'
import LinearGradLeft from '../../Common/linearGradLeft-css'

const Linear = Styled.div`
    ${LinearGrad};
`
const Linearleft = Styled.div`
    ${LinearGradLeft};
`

function data() {

    return (
        <div className="col-md-12">
            <Linear>
                <br /><br />
                <Left />
                <h1>User Policy</h1>
                <label>
                    This CueLab Acceptable Use Policy (“AUP”) applies to the use of any product, service or website provided by us (CueLab), whether we provide it directly or use another party to provide it to you (each, a “CueLab Service”). This AUP is designed to ensure compliance with the laws and regulations that apply to the CueLab Service. This AUP also protects the interests of all of our clients and their customers, as well as our goodwill and reputation.
                </label>
                <label>
                    These terms are so important that we cannot provide the CueLab Service unless you agree to them.
                    By using the CueLab Service, you are agreeing to following terms.
                </label>
                <label>
                    If you are using any CueLab Service, this AUP applies to you. Every client of ours agrees to abide by this AUP and is responsible for any violations. You are not allowed to assist or engage others in a way that would violate this AUP. We will enforce and ensure compliance with this AUP by using methods we consider to be appropriate, such as a complaint, monitoring your usage of our service, monitoring emails failures etc.
                </label>
                <label>
                    We periodically update these terms and we will let you know when we do through notification within the CueLab application used to access your CueLab subscription (if you have one), or by posting a revised copy on our website. You agree to review the AUP on a regular basis and always remain in compliance.
                </label>

            </Linear>
            {/* <Linearleft>
                <h3> Reporting Suspected Violations </h3>
                <label>
                    We encourage recipients of email messages sent using the CueLab Service to report suspected violations of this AUP to us by forwarding a copy of the received email with FULL headers to abuse@aidify.io
                <br />
                We encourage recipients affected by SMS messages sent using the CueLab Service to report suspected violations of this AUP to us by sending a screenshot of SMS the received message to abuse@aidify.io .
                <br />
                All other violations including, but not limited to, outbound calls (like telemarketing calls) can also be reported to abuse@aidify.io .
                <br />
                We have the policy to investigate all of these reports and to respond in the way we consider appropriate.
                </label>
                <Right />
            </Linearleft>
            <Linear>
                <h3> No SPAM Permitted </h3>
                <label>
                    You will not use the CueLab Service in any way (directly or indirectly) to send, transmit, handle, distribute or deliver:
                <br />
                1. unsolicited email (“spam” or “spamming”) in violation of the CAN-SPAM Act or any other law;
                <br />
                2. email to an address obtained via Internet harvesting methods or any surreptitious methods (e.g., scraping or harvesting);
                <br />
                3. email to purchased, rented, or borrowed lists;
                <br />
                4. email to an address that is incomplete, inaccurate and/or not updated for all applicable opt-out notifications, using best efforts and best practices in the industry
                <br />
                5. email to lists that are likely to result in an excessive number of unsubscribe requests or SPAM complaints or notices, as determined by acceptable industry practices.
                </label>
            </Linear>
            <Linearleft>
                <h3>Prohibited Email Content</h3>
                <label>
                    Email sent or caused to be sent to, or through the CueLab Service cannot:
                <br />
                1. use or contain invalid or forged headers;
                <br />
                2. use or contain invalid or non-existent domain names;
                <br />
                3. employ any technique to otherwise misrepresent, hide or obscure any information in identifying the point of origin or the transmission path;
                <br />
                4. use other means of deceptive addressing;
                <br />
                5. use a third party’s internet domain name without their consent, or be relayed from or through a third party’s equipment without the third party’s permission;
                <br />
                6. contain false or misleading information in the subject line or otherwise contain false or misleading content; or
                </label>
                <Left />
            </Linearleft>
            <Linear>
                <h3>Email Opt-out Requirements </h3>
                <label>
                    You warrant that each email you send or is sent for you using the CueLab Service will contain:
                <br />
                header information that is not false or misleading; and
                <br />
                an advisement that the recipient may unsubscribe, opt-out or otherwise demand that use of its information for unsolicited, impermissible and/or inappropriate communication(s) as described in this AUP be stopped (and how the recipient can notify you that it wants to unsubscribe, opt-out, or stop this use of its information). These requirements may not apply if the email sent is a transactional email and these requirements are not otherwise required by law. You warrant that you will promptly comply with all opt-out, unsubscribe, “do not call” and “do not send” requests.
                </label>
            </Linear>
            <Linearleft>
                <h3> Telephone and SMS Marketing</h3>
                <label>
                    You must comply with all local, national and relevant international laws relating to telephone and SMS marketing. You must comply with all laws related to the recording of phone calls and ensure all proper consent to record is obtained prior to making any such recording. You must comply with laws related to transactional and promotional messages applicable to the country of the recipient.
                <br />
                If you are based in India and use CueLab to make calls and send SMS to recipients in India then you must follow the laws formulated by Telecom Regulatory Authority of India (TRAI).
                </label>
                <Right />
            </Linearleft>
            <Linear>
                <h3>No Disruption</h3>
                <label>
                    You agree not to use the CueLab Service in a way that impacts the normal operation, privacy, integrity or security of another’s property.  Another’s property includes another’s account(s), domain name(s), URL(s), website(s), network(s), system(s), facilities, equipment, data, other information, or business operations.  You also agree not to use the CueLab Service to gain unauthorized access to, use, monitor, make an unauthorized reference to, another’s property unless you have the appropriate express prior consent to do so.  Examples of prohibited actions include (without limitation): hacking, spoofing, denial of service, mailbombing and/or sending any email that contains or transmits any virus or propagating worm(s), or any malware, whether spyware, adware or other such file or program. These restrictions apply regardless of your intent and whether or not you act intentionally or unintentionally.
                </label>
            </Linear>
            <Linearleft>
                <Left />
                <h3>Proper Usage of the CueLab Service</h3>
                <label>
                    You will respect the limits that apply to your use the CueLab Service. In addition, and without limiting the other requirements in this AUP, you may not (directly or indirectly) use the CueLab Service with content, or in a manner that:
                <br />
                1. is threatening, abusive, harassing, stalking, or defamatory;
                <br />
                2. is deceptive, false, misleading or fraudulent;
                <br />
                3. is invasive of another’s privacy or otherwise violates another’s legal rights (such as rights of privacy and publicity);
                <br />
                4. contains vulgar, obscene, indecent or unlawful material;
                <br />
                5. infringes a third party’s intellectual property right(s);
                <br />
                6. publishes, posts, uploads, or otherwise distributes any software, music, videos, or other material protected by intellectual property laws (or by rights of privacy or publicity) unless you have all necessary rights and consents to do so;
                <br />
                7. uploads files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another person’s computer;
                <br />
                8. downloads any file that you know, or reasonably should know, cannot be legally distributed in that way;
                <br />
                9. falsifies or deletes any author attributions, legal or proprietary designations, labels of the origin or source of software, or other material contained in a file that is uploaded;
                <br />
                10. restricts or inhibits any other user of the CueLab Service from using and enjoying our website and/or the CueLab Service;
                <br />
                11. harvests or otherwise collects information about others, including e-mail addresses, without their consent;
                <br />
                12. violates the usage standards or rules of an entity affected by your use, including without limitation any ISP, ESP, or news or user group (and including by way of example and not limitation circumventing or exceeding equipment use rights and restrictions and/or location and path identification detail);
                <br />
                You will use the CueLab Service for your internal business purposes and will not: (i) willfully tamper with the security of the CueLab Service or tamper with our customer accounts; (ii) access data on the CueLab Service not intended for you; (iii) log into a server or account on the CueLab Service that you are not authorized to access; (iv) attempt to probe, scan or test the vulnerability of any CueLab Service or to breach the security or authentication measures without proper authorization; (v) willfully render any part of the CueLab Service unusable; (vi) lease, distribute, license, sell or otherwise commercially exploit the CueLab Service or make the CueLab Service available to a third party other than as contemplated in your subscription to the CueLab Service; (vii) use the CueLab Service for timesharing or service bureau purposes or otherwise for the benefit of a third party; or (viii) provide to third parties any evaluation version of the CueLab Service without our prior written consent.
                </label>
            </Linearleft>
            <Linear>
                <Right />
                <h3>Compliance with applicable laws</h3>
                <label>
                    In addition, and without limiting the other requirements in this AUP, you may not (directly or indirectly) use the CueLab Service with content, or in a manner that is in violation of any applicable local, state, national or international law or regulation, including all export laws and regulations and without limitation:
                <br />
                1. Controlling the Assault of Non-Solicited Pornography and Marketing Act (CAN-SPAM Act) (15 U.S.C. § 7701 et seq.),
                <br />
                2. S Telephone Consumer Protection Act of 1991 (47 U.S.C. § 227),
                <br />
                3. India’s regulations on Unsolicited Commercial Communicated implemented by Telecom Regulatory Authority of India (TRAI) – http://www.nccptrai.gov.in/nccpregistry/
                <br />
                4. Do-Not-Call Implementation Act of 2003 (15 U.S.C. § 6152 et seq.; originally codified at § 6101 note),
                <br />
                5. Directive 2000/31/EC of the European Parliament and Council of 8 June 2000, on certain legal aspects of information society services, in particular, electronic commerce in the Internal Market (‘Directive on Electronic Commerce’), along with the Directive 2002/58/EC of the European Parliament and Council of 12 July 2002, concerning the processing of personal data and the protection of privacy in the electronic communications sector (‘Directive on Privacy and Electronic Communications’),
                <br />
                6. Personal Information Protection and Electronic Documents Act (PIPEDA) (S.C. 2000, c. 5),
                <br />
                7. Canada’s Anti-Spam Legislation (CASL) (S.C. 2010, c. 23)
                <br />
                8. Japan’s Act on Regulation of Transmission of Specified Electronic Mail (Act No. 26 of April 17, 2002)
                <br />
                9. any regulations having the force of law or laws in force in your or your email/SMS/phone call recipient’s country of residence.
                </label>
            </Linear>
            <Linearleft>
                <h3>General Terms</h3>
                <label>
                    If you breach of this AUP we may immediately suspend your access to the CueLab Service.  We may also terminate your and our subscription agreement for cause if you breach this AUP. You acknowledge we may disclose information regarding your use of any CueLab Service to satisfy any law, regulation, government request, court order, subpoena or other legal processes.  If we make this type of required disclosure we will notify you, unless we are required to keep the disclosure confidential.<br />We are not obligated to but may choose to, remove any prohibited materials and deny access to any person who violates this AUP. We further reserve all other rights.
                </label>
            </Linearleft> */}
        </div>
    )
}
export default data;