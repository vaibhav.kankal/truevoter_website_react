import React from 'react';
import './App.css';

import Nav from './components/Nav/nav'
import Route from './route/Route'
import Footer from './components/Footer/footer'

function App() {
  return (
    <div className="App">
      <Nav />
      <Route />
      <Footer /> 
    </div>
  );
}
export default App;
