import {CONTACT_US} from '../action/index'
const initialState = {
    data:[]
}
const rootReducer=(state=initialState,action)=>{
    if(action.type===CONTACT_US){
        return {
            ...state,
            data:action.payload
        }; 
    } else {
        return{
        ...state
        };
    }
}
export default rootReducer;