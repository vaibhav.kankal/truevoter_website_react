import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";

import Home from '../components/Index/index'
import VoterList from '../components/VoterList/index'
import Privacy from '../components/PrivacyPolicy/index'
import AboutUs from '../components/AboutUs/index'
import Blog from '../components/Blog/index'

function route () {
    return(
    <BrowserRouter>
        <Route exact path='/' component={Home} />
        <Route path="/Voter-list-search" component={VoterList} />
        <Route path="/privacy-policy" component={Privacy} />
        <Route path="/about-us" component={AboutUs} />
        <Route path="/blog" component={Blog} />
    </BrowserRouter>
    )
}
export default route;
